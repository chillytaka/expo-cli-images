FROM node:12-alpine

RUN addgroup expo \
      && adduser -G expo -s /bin/sh -D expo

RUN apk add --no-cache bash

USER expo

WORKDIR /home/expo

RUN yarn global add expo-cli

ENV PATH="$PATH:/home/expo/.yarn/bin"

ENTRYPOINT /bin/bash
